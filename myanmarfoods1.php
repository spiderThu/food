   <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>
	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Myanmar Foods</li>
		</ol>
	</div>
	<!-- //page details -->
	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Myanmar <span> Foods </span></h3>
				
			</div>

					
	
				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">တံခွန် မြန်မာထမင်းဆိုင်</h3>
					<p class="sub-para">အခန်​းအမှတ်​ B3/ B4၊ မဟာအံထူးကံသာဘုရား ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959426264954">09 426264954</a></p>

					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/tagon.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/sanpya.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">စံပြ မြန်မာထမင်းဆိုင်</h3>
					<p class="sub-para">မန္တလေး-လားရှိးလမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1849.1767127089747!2d96.48742087819369!3d22.03607200107918!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9eb5de92953d%3A0x7025382328ac74bb!2sSan%20Pya!5e0!3m2!1sen!2smm!4v1576882612121!5m2!1sen!2smm"frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
					
						
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Sein Si</h3>
					<p class="sub-para">တနတရှေ့၊ ဘုရင်မ ဆွယ်တာဆိုင်ဘေး၊ မန္တလေး-လားရှိးလမ်း၊ ပြင်ဦးလွင်မြို့။ </p>
					<p class="sub-para"><a href="tel:+959423839862">09 423839862</a></p>
					
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/seinsi.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/shwemandalay.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					<h3 class="title-sub mb-4">ရွှေမန္တလေး မြီးရှည်</h3>
					<p class="sub-para">Ruby Mart အတွင်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+9590944990991">09 0944990991</a></p>
					
					
				
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4"> Every Night</h3>
					<p class="sub-para">စံပြထမင်းဆိုင်ဘေး ၊ Ruby Mart အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959681063200">09 681063200</a></p>
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/everynight.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/myanmarrice.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
					
						<h3 class="title-sub mb-4">တော်ဝင်ရိုးရာ မြန်မာထမင်းဆိုင်</h3>
					<p class="sub-para">မန္တလေး-လားရှိးလမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:10AM–9PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1849.1010096329792!2d96.50068592289874!3d22.0418663846092!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9937057c74db%3A0xfed7984fe70270!2sTaw%20Win%20Yoe%20Yar%20Myanmar%20Food%20Centre!5e0!3m2!1sen!2smm!4v1576882759675!5m2!1sen!2smm"frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
					
				</div>
			</div>
		</div>
	</section>
	<!-- //about -->


	    <div class="pagination p1">
      <ul>
        <a href="myanmarfoods.php"><li><</li></a>
        <a href="myanmarfoods.php"><li>1</li></a>
        <a class="is-active" href="#"><li>2</li></a>
        <a href="myanmarfoods.php"><li>></li></a>
      </ul>
    </div>



	<?php include 'footer.php';?>

</body>

</html>
