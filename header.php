 	<!-- header -->
	<header id="home">
		<!-- top-bar -->
		<div class="top-bar py-2 border-bottom">
			<div class="container">
				<div class="row middle-flex">
					<div class="col-xl-7 col-md-5 top-social-agile mb-md-0 mb-1 text-lg-left text-center">
						<div class="row">
							<div class="col-xl-6 col-6 header-top_w3layouts">
								<p class="text-da">
									<span class="fa fa-map-marker mr-2"></span> Pyin Oo Lwin, Mandalay
								</p>
							</div>
							<div class="col-xl-6 col-6 header-top_w3layouts">
								<p class="text-da">
									<span class="fa fa-phone mr-2"></span>Spider Network Office Ph : +95 9977870421
								</p>
							</div>
							<div class="col-xl-6"></div>
						</div>
					</div>
					<div class="col-xl-5 col-md-7 top-social-agile text-md-right text-center pr-sm-0 mt-md-0 mt-2">
						<div class="row middle-flex">
							<div class="col-lg-5 col-4 top-w3layouts p-md-0 text-right">
						
							</div>
							<div class="col-lg-7 col-8 social-grid-w3">
								<!-- social icons -->
								<ul class="top-right-info">
									<li>
										<p>Follow Us:</p>
									</li>
									<li class="facebook-w3">
										<a href="https://www.facebook.com/spidernetworkgroup/" target="_blank">
											<span class="fa fa-facebook-f"></span>
										</a>
									</li>
								</ul>
								<!-- //social icons -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- //top-bar -->

	<!-- header 2 -->
		<!-- navigation -->
	<div class="main-top py-1">
		<div class="container">
			<div class="nav-content">
				<!-- logo -->
				<h1>
					<a id="logo" class="logo" href="index.php">
						<img src="images/logo.png" alt="" class="img-fluid"><span>Food Guide In</span> Pyin Oo Lwin
					</a>
				</h1>
				<!-- //logo -->
				<!-- nav -->
				<div class="nav_web-dealingsls">
					<nav>
						<label for="drop" class="toggle">Menu</label>
						<input type="checkbox" id="drop" />
						<ul class="menu">
							<li><a href="index.php">Home</a></li>				
							<li>
								<label for="drop-3" class="toggle toogle-2">Tradational Foods <span class="fa fa-angle-down"
										aria-hidden="true"></span>
								</label>
								<a href="#pages">Tradational Foods <span class="fa fa-angle-down" aria-hidden="true"></span></a>
								<input type="checkbox" id="drop-3" />
								<ul>
									<li><a class="drop-text" href="myanmarfoods.php">Myanmar Foods</a></li>
									<li><a class="drop-text" href="shanfoods.php">Shan Foods</a></li>
									<li><a class="drop-text" href="chinesefoods.php">Chinese Foods</a></li>
									
								</ul>
							</li>
							<li>
								<label for="drop-2" class="toggle toogle-2">Restaurants <span class="fa fa-angle-down"
										aria-hidden="true"></span>
								</label>
								<a href="#pages">Restaurants <span class="fa fa-angle-down" aria-hidden="true"></span></a>
								<input type="checkbox" id="drop-2" />
								<ul>
									<li><a class="drop-text" href="restaurants.php">Restaurants</a></li>
									<li><a class="drop-text" href="hotpot.php">Hot Pot</a></li>
								</ul>
							</li>
							<li><a href="cafes.php">Cafes</a></li>
								</li>
								<li>
								<label for="drop-1" class="toggle toogle-2">Street Foods <span class="fa fa-angle-down"
										aria-hidden="true"></span>
								</label>
								<a href="#pages">Street Foods <span class="fa fa-angle-down" aria-hidden="true"></span></a>
								<input type="checkbox" id="drop-1" />
								<ul>
									
										<li><a class="drop-text" href="streetfoods.php">Street Foods</a></li>
										<li><a class="drop-text" href="delivery.php">Delivery Services</a></li>
								</ul>
							</li>
							<li><a href="aboutus.php">About Us</a></li>
							<li>
							</li>
						</ul>
					</nav>
				</div>
				<!-- //nav -->
			</div>
		</div>
	</div>
	<!-- //navigation -->
	<!-- //header 2 -->