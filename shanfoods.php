   <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>

	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Shan Foods</li>
		</ol>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Shan <span> Foods </span></h3>
				
			</div>
			<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">မောဓနု</h3>
					<p class="sub-para">အမှတ်(၁၄၅)၊ ဘလော့ (၂၈) ၊ သုမင်္ဂလာရပ်ကွက် ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+95943062834">09 43062834</a></p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1099.5987987805213!2d96.47671463699562!3d22.02683330989497!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1e37cd489f35147f!2sMaw%20Danu!5e0!3m2!1sen!2smm!4v1577126876131!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/mawdanu.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/saungyanan.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">ဆောင်းရနံ့</h3>
					<p class="sub-para">ချယ်ရီလမ်းအဆုံး၊ဇမ္ဗူဒီပါလမ်း နှင့် ခတ္တာလမ်းထောင့်၊ မြို့သစ်၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959894828162">09 894828162</a></p>

				
				
				</div>
			</div>
				<br> <br> <br> <br> <br>
				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">ပင်လုံ</h3>
					<p class="sub-para">သုမင်္ဂလာစျေးလေး အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+95973500755">09 73500755</a></p>
					<p class="sub-para">Open Hours:7AM–7PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1849.2679899571303!2d96.47745900069486!3d22.02908363024798!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ebc7e36a18f%3A0xf0c541e0ce742a7b!2sPin%20Lon%20Restaurant!5e0!3m2!1sen!2smm!4v1577127168100!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/pinlon.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/shanfood.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">ရွှေကံ့ကော်</h3>
					<p class="sub-para"> ချယ်ရီလမ်း ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959894828162">09 894828162</a></p>
					
						
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">ကျိုင်းတုံ ရှမ်းရှိးရာ အစားအစာ</h3>
					<p class="sub-para">အိမ်အမှတ် (124/A)၊ သုမင်္ဂလာရပ်ကွက် ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+95973500755">09 73500755</a></p>
					<p class="sub-para">Open Hours:6AM–5PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10461.318832137047!2d96.47629611280902!3d22.02530199425601!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ebb60ba5723%3A0x36cc968fc18ac6b6!2z4YCA4YC74YCt4YCv4YCE4YC64YC44YCQ4YCv4YC24YCb4YC-4YCZ4YC64YC44YCb4YCt4YCv4YC44YCb4YCsIOGAheGArOGAuOGAnuGAseGArOGAgOGAuuGAhuGAreGAr-GAhOGAug!5e0!3m2!1sen!2smm!4v1577126776138!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
						
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/shantradational.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br>

	</section>
	<!-- //about -->







	<?php include 'footer.php';?>

</body>

</html>