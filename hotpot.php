   <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>

	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Chinese Foods</li>
		</ol>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Hot <span> Pot </span></h3>
				
			</div>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/hotpotgarden.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Hot Pot Garden</h3>
					<p class="sub-para">အမှတ်၆၈၊စန္ဒာလမ်း၊ရပ်ကွက်ကြီး(၅)၊စာပေ(၂)အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959251781138">09 251781138</a></p>
					<p class="sub-para">Open Hours: 11AM–10PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.9361748952983!2d96.45458851478145!3d22.0137563854732!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9c299a2dbe4b%3A0xae91256acd6030bd!2sHot%20Pot%20Garden!5e0!3m2!1sen!2smm!4v1576522637787!5m2!1sen!2smm"frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
						
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Like Korean Hot Pot</h3>
					<p class="sub-para">ချယ်ရီလမ်း၊ ပဒေသာမြို့သစ်၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959959828808">09 959828808</a></p>
					<p class="sub-para">Open Hours: 10AM–10PM</p>
					<br>
					<iframe  class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.8675662423107!2d96.46638552061644!3d22.016384778814913!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9def20e18c25%3A0x6c57acad0d1ee54b!2zTGlrZSDhgIDhgJThgLrhgJDhgLHhgKzhgLrhgJzhgLHhgLg!5e0!3m2!1sen!2smm!4v1576524272231!5m2!1sen!2smm"frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/like.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/uandme.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">U & Me Hot Pot</h3>
						<p class="sub-para">အကွက် ၂၈၊သုမဂ်လာရပ်ကွက် ၊ရွှေဘုံသာလမ်းနှင့်သုမဂ်လာလမ်းထောင့်၊ ဝေဟင်စတိုးအနီး၊ ပြင်ဦးလွင်မြို့။</p>
						<p class="sub-para"><a href="tel:+959265356900">09 265356900</a></p>
						<p class="sub-para">Open Hours: 12AM–9:30PM</p>
						<br>
						<iframe class="foodmaps"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.5620208427877!2d96.47282551478165!3d22.028086585465623!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ebcc5296a77%3A0x9223200bfb3dee4!2sU%20%26%20Me%20Chinese%20Hot%20Pot%20Restaurant!5e0!3m2!1sen!2smm!4v1576883508019!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

					
				
				</div>
			</div>
		
					
		<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Shwe Nagar Korean Hot Pot</h3>
					<p class="sub-para">မြို့ပတ်လမ်း၊မီးရထားသံလမ်းကျော်စစ်ဆေးရုံးလမ်းထောင့်၊ရပ်ကွက်ကြီး(၇)၊<br>အိမ်အမှတ်(၁)၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959798467945">09 798467945</a></p>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.2835670206964!2d96.4666572460203!3d22.038745694149167!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9eeb258094bd%3A0xe30c4daedc58ccc9!2sGolden%20Dragon!5e0!3m2!1sen!2smm!4v1576536113314!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/shwenagar.jpg" alt="about" class="img-fluid" />
				</div>
			</div>
						<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/hotpotbeiq.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">HOT PotbeiQ Restaurant</h3>
					<p class="sub-para">အမှတ်(၅)၊ ဂေါက်ကွင်းလမ်း၊ ပြင်ဦးလွင်မြို့။ </p>
					<p class="sub-para"><a href="tel:+959400437665">09 400437665</a></p>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2615.485235037317!2d96.46383805597465!3d22.01687797325992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9e9d9bcfeb87%3A0x4f1c731e80cbe08e!2sHOT%20Hotpot%20%26%20Barbecue%20Restaurant!5e0!3m2!1sen!2smm!4v1576537043065!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

						
				
				</div>
			</div>
				<br> <br> <br> <br> <br>
				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Number One</h3>
					<p class="sub-para">Cb bank (2​)ရှေ့၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959798467945">09 256134996</a></p>
					
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/numberone.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

	</section>
	<!-- //about -->


		    <div class="pagination p1">
      <ul>
        <a href="hotpot1.php"><li><</li></a>
        <a class="is-active" href="#"><li>1</li></a>
        <a href="hotpot1.php"><li>2</li></a>
        <a href="hotpot1.php"><li>></li></a>
      </ul>
    </div>




	<?php include 'footer.php';?>

</body>

</html>