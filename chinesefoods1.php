    <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>

	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Chinese Foods</li>
		</ol>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Chinese <span> Foods </span></h3>
				
			</div>
			<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">ခေတ်စတား</h3>
					<p class="sub-para">မြို့လည် ရွှေစည်းခုံ ကျောင်းတိုက်အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/kitstar.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/seinpan.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					<h3 class="title-sub mb-4">စိန်ပန်း</h3>
					<p class="sub-para">တရုတ်ဘုံကျောင်းအနီး၊ ပြင်ဦးလွင်မြို့။</p>

						
				
				</div>
			</div>
				<br> <br> <br> <br> <br>
				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Smart</h3>
					<p class="sub-para"> Ruby Mart အတွင်း ၊ ပြင်ဦးလွင်မြို့။</p>
					
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/smart.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/shwenan.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Shwe Nan</h3>
					<p class="sub-para">အမှတ်(၁၂၉)စက်မှုလက်မှုသိပ္ပံကျောင်းထောင့်၊NELC ကျောင်းအနီး၊မြို့ပါတ်လမ်း ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959964008106">09 692136602</a></p>
					
						
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Lin Lae</h3>
					<p class="sub-para"> မြမာလာ ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959975345224">09 975345224</a></p>
						
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/linlae.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
		
		
					
				</div>
			</div>
		</div>
	</section>
	<!-- //about -->


		    <div class="pagination p1">
      <ul>
        <a href="chinesefoods.php"><li><</li></a>
        <a  href="chinesefoods.php"><li>1</li></a>
        <a class="is-active" href="#"><li>2</li></a>
        <a href="chinesefoods.php"><li>></li></a>
      </ul>
    </div>




	<?php include 'footer.php';?>

</body>

</html>