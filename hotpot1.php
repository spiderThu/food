   <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>

	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Chinese Foods</li>
		</ol>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Hot <span> Pot </span></h3>
				
			</div>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/mrpoem.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Mr.Poem</h3>
					<p class="sub-para">ဆသုံးလုံးလမ်းနဲ့ ရွှေမြင်တင်ဘုရားလမ်းထောင့်၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959964008106">09 964008106</a></p>
					<p class="sub-para">Open Hours:11AM-8PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d924.5784775975362!2d96.48107912919461!3d22.037584399091283!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9eb810a9c263%3A0xe94459405a1a6e59!2sPyin%20Oo%20Lwin!5e0!3m2!1sen!2smm!4v1576883780754!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
						
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Big Boss BBQ</h3>
					<p class="sub-para">B လမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959975345224">09 975345224</a></p>
						
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/bigboss.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/april19.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">April 19</h3>
					<p class="sub-para">မန္တလေး-လားရှိုးလမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959791986789">09 791986789</a></p>
					<br>
					
				
				</div>
			</div>
				<br> <br> <br> <br> <br>

				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">QQ</h3>
					<p class="sub-para"> ချယ်ရီလမ်း ၊ ပြင်ဦးလွင်မြို့။</p>
					
					
					
					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/qq.jpg" alt="about" class="img-fluid" />

					
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/winstar.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
					
					<h3 class="title-sub mb-4">Win Star</h3>
					<p class="sub-para">(၁၈)လမ်း၊ ပြင်ဦးလွင်မြို့။ </p>
					<p class="sub-para"><a href="tel:+959428777758">09 428777758</a></p>
					
					
					
					
						
				</div>
			</div>
	</section>
	<!-- //about -->


		    <div class="pagination p1">
      <ul>
        <a href="hotpot.php"><li><</li></a>
        <a href="hotpot.php"><li>1</li></a>
        <a class="is-active" href="hotpot1.php"><li>2</li></a>
        <a href="hotpot.php"><li>></li></a>
      </ul>
    </div>




	<?php include 'footer.php';?>

</body>

</html>