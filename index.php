  <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link href="css/css_slider.css" type="text/css" rel="stylesheet" media="all">
	<!-- css slider -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>

	<?php include 'header.php';?>
	
	<!-- banner -->
	<div class="baneer-w3ls">
		<div class="row no-gutters">
			<div class="col-xl-5 col-lg-6">
				<div class="banner-left-w3">
					<div class="container">
						<div class="banner-info_agile_w3ls">
							<!-- <h5>Only Fresh Burgers</h5> -->
							<h3 class="text-da mb-4">Food Guide In<span> Pyin Oo Lwin</span> </h3>
							<p> ပြင်ဦးလွင်သို့ လာရောက်လည်ပတ်ကြမည့် ရပ်နီးရပ်ဝေးမှ မိတ်ဆွေများအတွက် ပြင်ဦးလွင်မြို့ရှိ စားသောက်ဆိုင်များကို Spider Network ICT မှ <br>ယခု Website တွင် စုစည်းဖော်ပြပေးထားပါသည်။</p>
						
							<a href="aboutus.php" class="button-w3ls mt-5 ml-2"> ပိုမို လေ့လာရန်
								<span class="fa fa-caret-right ml-1" aria-hidden="true"></span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-7 col-lg-6 callbacks_container">
				<!-- banner slider -->
				<div class="csslider infinity" id="slider1">
					<input type="radio" name="slides" checked="checked" id="slides_1" />
					<input type="radio" name="slides" id="slides_2" />
					<input type="radio" name="slides" id="slides_3" />
					<ul class="banner_slide_bg">
						<li>
							<div class="banner-top1"></div>
						</li>
						<li>
							<div class="banner-top2"></div>
						</li>
						<li>
							<div class="banner-top3"></div>
						</li>
					</ul>
					<div class="arrows">
						<label for="slides_1"></label>
						<label for="slides_2"></label>
						<label for="slides_3"></label>
					</div>
					<div class="navigation">
						<div>
							<label for="slides_1"></label>
							<label for="slides_2"></label>
							<label for="slides_3"></label>
						</div>
					</div>
				</div>
				<!-- //banner slider -->
			</div>
		</div>
	</div>
	<!-- //banner -->
	<div class="clearfix"></div>

	<!-- about -->
	<!-- <div class="about-bottom">
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-6 about">
			
			</div>
			<div class="col-lg-7 col-md-6 about-right-w3 text-right py-md-5">

				<div class="right-space py-xl-5 py-lg-3">
					<div class="title-section mb-4">
				
						<h3 class="w3ls-title">iHost </h3>
					</div>
					<p class="about-text">မိတ်ဆွေတို့ရဲ့ စီးပွားရေးလုပ်ငန်းများ Internet ပေါ်တွင် <br> ပိုမိုအောင်မြင်စေရန်
									iHost မှ တာဝန်ယူဆောင်ရွက်ပေးပါရစေ...</p>
					<a href="https://ihost.spidernetworkict.com" target="_blank"  class="button-w3ls mt-5" >ပိုမို လေ့လာရန်
						<span class="fa fa-caret-right ml-1" aria-hidden="true"></span>
					</a>
				</div>
			</div>
		</div>
	</div> -->
	<!-- //about -->





	<!-- specials -->
<section class="blog_w3ls py-5">
		<div class="container pb-xl-5 pb-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<p class="w3ls-title-sub">အရသာရှိသော</p>
				<h3 class="w3ls-title">စားသောက်ဆိုင်<span>များ</span></h3>
			</div>
			<div class="row">
				<!-- blog grid -->
				<div class="col-lg-4 col-md-6">
					<div class="card border-0 med-blog">
						<div class="card-header p-0">
							<a href="myanmarfoods.php">
								<img class="card-img-bottom" src="img/myanmarfoods.jpg" alt="Card image cap">
							</a>
						</div>
						<div class="card-body border border-top-0">
							<h5 class="blog-title card-title m-0"><a href="myanmarfoods.php">Myanmar Foods</a></h5>
							<p class="mt-3">မြန်မာ ထမင်းဆိုင်များကို စုစည်းဖော်ပြထားပါသည်။</p>
							<a href="myanmarfoods.php" class="btn button-w3ls mt-4 mb-3">အသေးစိတ်ကြည့်ရန်
								<span class="fa fa-caret-right ml-1" aria-hidden="true"></span>
							</a>
						</div>
				
					</div>
				</div>
				<!-- //blog grid -->
				<!-- blog grid -->
				<div class="col-lg-4 col-md-6 mt-md-0 mt-5">
					<div class="card border-0 med-blog">
						<div class="card-header p-0">
							<a href="shanfoods.php">
								<img class="card-img-bottom" src="img/shanfoods.jpg" alt="Card image cap">
							</a>
						</div>
						<div class="card-body border border-top-0">
							<h5 class="blog-title card-title m-0"><a href="shanfoods.php">Shan Foods</a></h5>
							<p class="mt-3">ရှမ်းရှိးရာ စားသောက်ဆိုင်များကို စုစည်းဖော်ပြထားပါသည်။</p>
							<a href="shanfoods.php" class="button-w3ls active mt-4 mb-3">အသေးစိတ်ကြည့်ရန်
								<span class="fa fa-caret-right ml-1" aria-hidden="true"></span>
							</a>
						</div>
					</div>
				</div>
				<!-- //blog grid -->
				<!-- blog grid -->
				<div class="col-lg-4 col-md-6 mt-lg-0 mt-5">
					<div class="card border-0 med-blog">
						<div class="card-header p-0">
							<a href="chinesefoods.php">
								<img class="card-img-bottom" src="img/chinesefoods.jpg" alt="Card image cap">
							</a>
						</div>
						<div class="card-body border border-top-0">
							<h5 class="blog-title card-title m-0"><a href="chinesefoods.php">Chinese Foods</a></h5>
							<p class="mt-3">တရုတ်အစားအသောက် များကို စုစည်းဖော်ပြထားပါသည်။</p>
							<a href="chinesefoods.php" class="button-w3ls  mt-4 mb-3">အသေးစိတ်ကြည့်ရန်
								<span class="fa fa-caret-right ml-1" aria-hidden="true"></span>
							</a>
						</div>
					</div>
				</div>
				<!-- //blog grid -->
			</div>
			<br><br>
			<div class="row">
				<!-- blog grid -->
				<div class="col-lg-4 col-md-6">
					<div class="card border-0 med-blog">
						<div class="card-header p-0">
							<a href="restaruants.php">
								<img class="card-img-bottom" src="img/restaruants.jpg" alt="Card image cap">
							</a>
						</div>
						<div class="card-body border border-top-0">
							<h5 class="blog-title card-title m-0"><a href="restaruants.php">Restaruants</a></h5>
							<p class="mt-3">အစားအစာမျိုးစုံရရှိနိုင်သည့်စားသောက်ဆိုင်များ ကိုစုစည်းဖော်ပြထားပါသည်။</p>
							<a href="restaruants.php" class="btn button-w3ls mt-4 mb-3">အသေးစိတ်ကြည့်ရန်
								<span class="fa fa-caret-right ml-1" aria-hidden="true"></span>
							</a>
						</div>
				
					</div>
				</div>
				<!-- //blog grid -->
				<!-- blog grid -->
				<div class="col-lg-4 col-md-6 mt-md-0 mt-5">
					<div class="card border-0 med-blog">
						<div class="card-header p-0">
							<a href="cafes.php">
								<img class="card-img-bottom" src="img/cafes.jpg" alt="Card image cap">
							</a>
						</div>
						<div class="card-body border border-top-0">
							<h5 class="blog-title card-title m-0"><a href="cafes.php">Cafes</a></h5>
							<p class="mt-3">ကော်ဖီဆိုင်များနှင့် Bakery များကို စုစည်းဖော်ပြထားပါသည်။</p>
							<a href="cafes.php" class="button-w3ls active mt-4 mb-3">အသေးစိတ်ကြည့်ရန်
								<span class="fa fa-caret-right ml-1" aria-hidden="true"></span>
							</a>
						</div>
					</div>
				</div>
				<!-- //blog grid -->
				<!-- blog grid -->
				<div class="col-lg-4 col-md-6 mt-lg-0 mt-5">
					<div class="card border-0 med-blog">
						<div class="card-header p-0">
							<a href="streetfoods.php">
								<img class="card-img-bottom" src="img/streetfoods.jpg" alt="Card image cap">
							</a>
						</div>
						<div class="card-body border border-top-0">
							<h5 class="blog-title card-title m-0"><a href="streetfoods.php">Street Foods</a></h5>
							<p class="mt-3">အရသာကောင်းမွန်သော Street Foods များ နှင့် Mart များ ကို စုစည်းဖော်ပြထားပါသည်။</p>
							<a href="streetfoods.php" class="button-w3ls mt-4 mb-3">အသေးစိတ်ကြည့်ရန်
								<span class="fa fa-caret-right ml-1" aria-hidden="true"></span>
							</a>
						</div>
					</div>
				</div>
				<!-- //blog grid -->
			</div>

		</div>
		<br><br><br>

	</section>





	<?php include 'footer.php';?>
</body>
</html>