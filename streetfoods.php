 <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>

	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Street Foods</li>
		</ol>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Street <span> Foods </span></h3>
				
			</div>
			<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Night Market</h3>
					<p class="sub-para">စာပန်းအိမ် စာသင်ကျောင်းအနီး၊ ပိတောက်လမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:5PM-10PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d653.8302270589782!2d96.4806010500732!3d22.02577853820962!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9f24e307c6a3%3A0x33c1693daa41e14d!2sPyinOoLwin%20NightMarket!5e0!3m2!1sen!2smm!4v1577129269358!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/nightmarket.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/nightmarkett.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">ဒေါ်ထွေးခင် ညစျေး</h3>
					<p class="sub-para">အောင်ချမ်းသာဘုန်းကြီးကျောင်းအကျော် စျေးလေးမီးပွိုင့်အနီး၊ ယူနန်ဘုံကျောင်းလမ်းထိပ်၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:5PM-12PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.438127398729!2d96.47354341478177!3d22.03282978546311!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ebc1fa4cdef%3A0xdac5908b22c0f110!2sCircular%2C%20Maymyo!5e0!3m2!1sen!2smm!4v1578424848989!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					

						
				
				</div>
			</div>
				<br> <br> <br> <br> <br>
				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Nation Mart</h3>
					<p class="sub-para"> ဂေါက်ကွင်းလမ်း ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:6AM-11PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1099.6831997435577!2d96.46261114901812!3d22.015960475272433!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9d7e1c8a3e87%3A0x69a700deb7f1e42e!2sNation%20Mart!5e0!3m2!1sen!2smm!4v1577129590733!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/nationmart.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/star.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">တာရာမင်း Store</h3>
					<p class="sub-para">မန္တလေး-လားရှိုးလမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:8AM–8:30PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.459281350009!2d96.47179731478171!3d22.03201998546357!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9fa16dff19ff%3A0x9a9f071d46232b0b!2sTAR%20YAR%20MIN%20Mart!5e0!3m2!1sen!2smm!4v1577129645821!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
						
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Asia Store</h3>
					<p class="sub-para"> သုမင်္ဂလာစျေးလေးအနီး ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:8AM–8PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d549.7799668185673!2d96.47543321999447!3d22.031838342405276!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ebc3e2d6ffb%3A0xe4111a888daf3487!2sAsia%20Store!5e0!3m2!1sen!2smm!4v1577129896711!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
						
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/asia.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/sky.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">ဝေဟင် Store</h3>
					<p class="sub-para">အ.ထ.က(၁) နောက်ကျောလမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:7:30AM–7:30PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1099.5865623563109!2d96.47349499548898!3d22.028409226652162!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ebd43e807ff%3A0xe2c77774d5818ba3!2sWai%20Hin%20Store!5e0!3m2!1sen!2smm!4v1577130001487!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				
				
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Uni Mart</h3>
					<p class="sub-para">မန္တလေး-လားရှိုးလမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:8AM–8:30PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.4593988989213!2d96.47171581478172!3d22.032015485463578!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ebdc9361b21%3A0xddd0183779d5af6!2sUni%20Mart!5e0!3m2!1sen!2smm!4v1577130060718!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/unimart.jpg" alt="about" class="img-fluid" />
				</div>
			</div>
		</div>
		<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/rubymart.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Ruby Mart</h3>
					<p class="sub-para">မန္တလေး-လားရှိုးလမ်း၊ DSTA အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:9AM–9PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.2998405193152!2d96.48751191478188!3d22.03812288546039!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9eb588d30c13%3A0x9f0fd0bb0ec9e335!2sRuby%20Mart!5e0!3m2!1sen!2smm!4v1577129418267!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

						
				
				</div>
			</div>

	</section>
	<!-- //about -->






	<?php include 'footer.php';?>

</body>

</html>