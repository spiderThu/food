    <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>

	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Restaurants</li>
		</ol>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Restau<span>rants </span></h3>
				
			</div>
			<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
				<h3 class="title-sub mb-4">1982 Shishr Bar</h3>
					<p class="sub-para">(၁၂)လမ်းထောင့်၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+95943186723">09 43186723</a></p>
					<p class="sub-para">Open Hours:4PM-11PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.4848672919607!2d96.47796601478171!3d22.031040485464114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9fec4768ab17%3A0x58d71f9149e9af5c!2s1982%20Shisha%20Bar%20%26%20BBQ!5e0!3m2!1sen!2smm!4v1576884450021!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/1982.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/supon.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">စုပေါင်း</h3>
					<p class="sub-para">တရုတ်ဘုံကျောင်းအနီး ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:6AM–8:30PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1307.67590742267!2d96.47718045161606!3d22.024104779601736!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ea220f9c8e9%3A0x7eb13f408a860086!2sSu%20Paung%20Restaurant%26ktv!5e0!3m2!1sen!2smm!4v1576884581579!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">ပန်းတော်ဝင်</h3>
					<p class="sub-para">နန္ဒာလမ်း၊ ကန်တော်ကြီးဥယျာဉ်အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959964005767">09 964005767</a></p>
					<p class="sub-para">Open Hours:7AM–10PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4398.983283233753!2d96.47068298346133!3d22.007890106592445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9c218cfed869%3A0x299d0747e71287ef!2sPan%20Taw%20Win!5e0!3m2!1sen!2smm!4v1576884642117!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/pantawwin.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/friday.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Friday BBQ And Restaurant</h3>
					<p class="sub-para">ကန်တော်လေး ပတ်လမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959444864486">09 444864486</a></p>
					<p class="sub-para">Open Hours:9AM–10PM</p>
					<br>
					<iframe class="foodmaps"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.9416144037737!2d96.4677173147814!3d22.013547985473327!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9d81540f3417%3A0x18a085c7a556d227!2sFriday%20BBQ%20%26%20BAR!5e0!3m2!1sen!2smm!4v1576884693619!5m2!1sen!2smm"frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				
				</div>
			</div>
						<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
				<h3 class="title-sub mb-4">The Grey House</h3>
					<p class="sub-para">အမှတ်(၃၃)၊ မြို့ပတ်လမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959952844653">09 952844653</a></p>
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/thegrayhouse.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/championskybar.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
						<h3 class="title-sub mb-4">Champion Sky Bar</h3>
					<p class="sub-para">အမှတ်(၂၆)၊ နန္ဒာလမ်း၊ ကန်တော်ကြီးဥယျာဉ်အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959426370070">09 426370070</a></p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3698.5500454473563!2d96.4817462!3d22.0285451!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9eb053b58171%3A0x142a08f6874dc457!2sCHAMPION%20KTV!5e0!3m2!1sen!2smm!4v1578421703054!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				
				</div>
			</div>
				<br> <br> <br> <br> <br>
				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">ထီးလှိုင်စားသောက်ဆိုင်</h3>
					<p class="sub-para">ကန်တော်လေးလမ်း၊ အဝိုင်းပတ်နား၊ ပြင်ဦးလွင်မြို့။</p>
					
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.8790884264595!2d96.46663891494966!3d22.015943385472056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9e9e2612a9ed%3A0x44512ab8e3514439!2sHtee%20Hline!5e0!3m2!1sen!2smm!4v1578421344354!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe></iframe>
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/hteehline.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/cherrycity.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
						<h3 class="title-sub mb-4">Cherry City Bar</h3>
					<p class="sub-para">အမှတ်(၁၁) ၊A လမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959890405050">09 890405050</a></p>
					<br>
					<iframe class="foodmaps"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.4423410070976!2d96.47255951478179!3d22.032668485463258!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9f9e6720332f%3A0x4f83e9b5c0879f99!2sCherry%20City%20Bar!5e0!3m2!1sen!2smm!4v1576884910404!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
				
				
				</div>
			</div>


	</section>
	<!-- //about -->


	<div class="pagination p1">
      <ul>
        <a href="restaurants.php"><li><</li></a>
        <a href="restaurants.php"><li>1</li></a>
        <a class="is-active" href="#"><li>2</li></a>
        <a href="restaurants.php"><li>></li></a>
      </ul>
    </div>




	<?php include 'footer.php';?>

</body>

</html>