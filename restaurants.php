    <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>

	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Restaurants</li>
		</ol>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Restau<span>rants </span></h3>
				
			</div>
			<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Feel</h3>
					<p class="sub-para">ကန်တော်လေးအနီး၊ နန္ဒာလမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+9595162132">09 5162132</a></p>
					<br>
					<iframe class="foodmaps"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.9026754233!2d96.46044411478145!3d22.015039785472478!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9dd630f17dd9%3A0x5e62b47af1ff10c8!2sFeel%20Cafe!5e0!3m2!1sen!2smm!4v1576883880715!5m2!1sen!2smm"frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/feel.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/thetaj.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
				<h3 class="title-sub mb-4">The Taj</h3>
					<p class="sub-para">အမှတ်(၂၆)၊ နန္ဒာလမ်း၊ ကန်တော်ကြီးဥယျာဉ်အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959784049880">09 784049880</a></p>
					<p class="sub-para">Open Hours:10AM–10PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.9736240323623!2d96.46725921478142!3d22.01232158547391!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9c204ee54903%3A0x8335fb97464d4950!2sThe%20Taj%20Pyin%20Oo%20Lwin!5e0!3m2!1sen!2smm!4v1576883948954!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				
				</div>
			</div>
				<br> <br> <br> <br> <br>
				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">High Way King</h3>
					<p class="sub-para">ရပ်ကွက်ကြီး(၁) ၊မြို့ပတ်လမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959255598976">09 255598976</a></p>
					<p class="sub-para">Open Hours:6AM–9PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.3549908041837!2d96.47101431478181!3d22.036012085461522!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9f9c51161197%3A0x7b3be793fc1db0f4!2sHighway%20King%20Indian%20Restaurant!5e0!3m2!1sen!2smm!4v1576884108403!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/highwayking.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/royallake.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Royal Lake Restaurant</h3>
					<p class="sub-para">ချယ်ရီလမ်းနှင့်ပိတောက်လမ်းထောင့်၊ ပဒေသာမြို့သစ်၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959441009445">09 441009445</a></p>
					<p class="sub-para">Open Hours:7AM–10PM</p>
					<br>
					<iframe class="foodmaps"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.5222607886!2d96.48225811478173!3d22.02960888546485!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9fa8bc68f57f%3A0x4ac50b1b825bda72!2sRoyal%20Lake%20Restaurant!5e0!3m2!1sen!2smm!4v1576884184643!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
						<h3 class="title-sub mb-4">December</h3>
					<p class="sub-para">မန္တလေး-လားရှိးလမ်း၊ မဟာအံ့ထူးကံသာ ဘုရားကြီး အနီး ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959964005767">09 964005767</a></p>
					<p class="sub-para">Open Hours:6AM–6PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3697.769584854693!2d96.55161901478225!3d22.05840788544976!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc99b4dc45c20d%3A0xb78d037a940f4977!2sDecember%20Garden%20%26%20Waterfall!5e0!3m2!1sen!2smm!4v1576884302651!5m2!1sen!2smm"frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/december.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/heaven.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Heaven Bar And Restaurant</h3>
				<p class="sub-para">အမှတ် (၅၄)၊ စန္ဒာလမ်း၊ စာပေ(၂)အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959675717724">09 675717724</a></p>
					<p class="sub-para">Open Hours:8AM–9PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.9499406199516!2d96.45472331478139!3d22.013228985473386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ded6b1a95ab%3A0xe9e4ae2715f61215!2sHeaven%20Bar%26Restaurant!5e0!3m2!1sen!2smm!4v1576884386285!5m2!1sen!2smm"frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">AE86 Car Detailing & Bar</h3>
					<p class="sub-para">မြို့ပတ်လမ်းနှင့်မြစန္ဒာလမ်းဒေါင့် ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959773338854">09 773338854</a></p>
					<p class="sub-para">Open Hours:8AM–12PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1555.1370712300181!2d96.47104049256983!3d22.02049967958054!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9e9f574e51ab%3A0xb0a46b727811fafc!2sMya%20Nandar%20St%2C%20Maymyo!5e0!3m2!1sen!2smm!4v1578423282284!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/ae86.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/dreamzone.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
				<h3 class="title-sub mb-4">Dream Zone Bar</h3>
					<p class="sub-para">မြို့ပတ်လမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959794402914">09 794402914</a></p>
					<p class="sub-para">Open Hours:10AM–11PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.445261519445!2d96.47405161478174!3d22.03255668546328!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9f8871b321bd%3A0x5dd335157638995e!2sDream%20Zone%20Bar!5e0!3m2!1sen!2smm!4v1578423352370!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				
				</div>
			</div>

	</section>
	<!-- //about -->


	<div class="pagination p1">
      <ul>
        <a href="restaurants1.php"><li><</li></a>
        <a class="is-active" href="#"><li>1</li></a>
        <a href="restaurants1.php"><li>2</li></a>
        <a href="restaurants1.php"><li>></li></a>
      </ul>
    </div>
<?php include 'footer.php';?>
</body>
</html>