   <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>

	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Cafes</li>
		</ol>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Ca<span>fes </span></h3>
				
			</div>
			<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Bamboo Land</h3>
					<p class="sub-para">Corner of Mdy-Lao Rd & Theingi St, Back of Myanandar Hotel၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+9592045247">09 444665556</a></p>
				
				

					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/bambooland.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/pizzafamily.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					<h3 class="title-sub mb-4">Pizza Family</h3>
					<p class="sub-para">အမှတ်(၂၅၀)၊ချယ်ရီလမ်းမကြီးနှင့် ၁ရလမ်းထောင့်၊ရ.က.က (၂)၊Crown Hotel အောက်၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+95933144369">09 33144369</a></p>

						
				
				</div>
			</div>
				<br> <br> <br> <br> <br>
				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Carnio</h3>
					<p class="sub-para">သစ်တောလမ်း ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:8AM–9PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.7167710595113!2d96.47449311478155!3d22.022160685468766!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ea3f431e9e1%3A0x8f5ce79bdde3ce4a!2sCarino%20Cafe%20%26%20Resturant!5e0!3m2!1sen!2smm!4v1577128895420!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
					

					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/carnio.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/peacelily.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Peace Lily</h3>
					<p class="sub-para">အောင်တိုက်တန်း အခန်း(၇) ၊ တရုတ်ဘုံကျောင်းအနီး ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959420033828"> </a></p>
					<p class="sub-para">Open Hours:9AM–7PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1555.1175404441954!2d96.4776816191539!3d22.022278779102596!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9f23b93d62fd%3A0x8729115d27a45311!2sPeace%20Lily%20restaurant!5e0!3m2!1sen!2smm!4v1577129003214!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
						
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Sam Cafe</h3>
					<p class="sub-para"> မြို့ပတ်လမ်း ၊ စုပေါင်း လက်ဖက်ရည်ဆိုင်အနီး ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959420107538">09 420107538</a></p>
					<p class="sub-para">Open Hours:8AM–9PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1849.3372149530855!2d96.47680028875878!3d22.02378221948598!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe72095d5265671c2!2sSam%20Restaurant!5e0!3m2!1sen!2smm!4v1577129046928!5m2!1sen!2smm"frameborder="0" style="border:0;" allowfullscreen=""></iframe>
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/sam.jpg" alt="about" class="img-fluid" />
				</div>
			</div>



			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/maymyoeain.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4"> မေမြို့အိမ် </h3>
					<p class="sub-para"> ပြင်ဦးလွင် မြို့အဝင်၊ ကျောက်တောင်ဘုရားအနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959795183392">09 795183392</a></p>
					<p class="sub-para">Open Hours:8AM–10PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.984463177565!2d96.43959621478136!3d22.01190628547405!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9dd723143d9f%3A0xdf84aa703007ac59!2sMay%20Myo%20Eain!5e0!3m2!1sen!2smm!4v1577129135381!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				
					
				</div>
			</div>
		</div>
	</section>
	<!-- //about -->


		    <div class="pagination p1">
      <ul>
        <a href="cafes1.php"><li><</li></a>
        <a href="cafes.php"><li>1</li></a>
        <a href="cafes1.php"><li>2</li></a>
        <a class="is-active" href="#"><li>3</li></a>
        <a href="cafes.php"><li>></li></a>
      </ul>
    </div>




	<?php include 'footer.php';?>

</body>

</html>