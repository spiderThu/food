  <!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
	<title>Tasty Burger Restaurants Category Bootstrap Responsive Web Template | About Us :: W3layouts</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Tasty Burger Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>

<?php include 'header.php';?>

	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">About Us</li>
		</ol>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3">About <span>Us</span></h3>
				<p class="titile-para-text mx-auto">ပြင်ဦးလွင်သို့ လာရောက်လည်ပတ်ကြမည့် ရပ်နီးရပ်ဝေးမှ မိတ်ဆွေများအတွက် ပြင်ဦးလွင်မြို့ရှိ စားသောက်ဆိုင်များကို Spider Network ICT မှ <br>ယခု Website တွင် စုစည်းဖော်ပြပေးထားပါသည်။</p>
			</div>
				<!-- specials -->
<section class="blog_w3ls py-5">
		<div class="container pb-xl-5 pb-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<!-- <p class="w3ls-title-sub">Tasty</p> -->
				<h3 class="w3ls-title"> Spider Network  <span>ICT</span></h3>
			</div>
			<div class="row">
				<!-- blog grid -->
				<div class="col-lg-4 col-md-6">
					<div class="card border-0 med-blog">
						<div class="card-header p-0">
							<a href="menu.php">
								<img class="card-img-bottom" src="img/s1.jpg" alt="Card image cap">
							</a>
						</div>
					
				
					</div>
				</div>

				<!-- //blog grid -->
				<!-- blog grid -->
				<div class="col-lg-4 col-md-6 mt-md-0 mt-5">
					<div class="card border-0 med-blog">
						<div class="card-header p-0">
							<a href="menu.php">
								<img class="card-img-bottom" src="img/s2.jpg" alt="Card image cap">
							</a>
						</div>
						
					</div>
				</div>
				<!-- //blog grid -->
				<!-- blog grid -->
				<div class="col-lg-4 col-md-6 mt-lg-0 mt-5">
					<div class="card border-0 med-blog">
						<div class="card-header p-0">
							<a href="menu.php">
								<img class="card-img-bottom" src="img/s3.jpg" alt="Card image cap">
							</a>
						</div>
						
					</div>
				</div>
				<!-- //blog grid -->
			</div>
		</div>
	</section> 
	<!-- //specials -->
			<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h5 class="title-sub mb-4">Spider Network မှ ဝန်ဆောင်မှုပေးနေသော လုပ်ငန်းများ</h5>
					<p class="sub-para">

<ul style="text-align:left;line-height:30px;list-style-type:none;color:#999;">
<li>     <img src="img/about-icon.png"> HDCVI, IP & Analog-CCTV Security System </li>
<li>     <img src="img/about-icon.png"> Network Infrastructure Solution		 </li>
<li>     <img src="img/about-icon.png"> Time Attendance & Access Control System </li>
<li>     <img src="img/about-icon.png"> IP-PABX & Fire Alarm System			 </li>
<li>     <img src="img/about-icon.png"> MATV & PA System						 </li>
<li>     <img src="img/about-icon.png"> Car GPS Tracking System				 </li>
<li>     <img src="img/about-icon.png"> Web Design & Development				 </li>
</ul>


				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/services.jpg" alt="about" class="img-fluid" />
				</div>
			</div>
		</div>
	</section>
	<!-- //about -->

	<!-- services -->
	<section class="middle py-5" id="services">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
			<img src="img/ihostlogo.png" alt="" class="img-fluid" style="width:25%;padding-top:30px;">
			<br><br><br>
			<p class="w3ls-title-sub" style="color:white;">မိတ််ဆွေတို့ရဲ့ လုပ်ငန်းများ Internet ပေါ်တွင် ပိုမိုကျယ်ပြန့်စွာ လုပ်ဆောင်နိုင်ရန်အတွက် Webiste များကို <br/><b> iHost  </b>တွင် ယုံကြည်စိတ်ချစွာ
			   အပ်နှံနိုင်ပါပြီ... 
			<b>iHost</b> က မိတ်ဆွေတို့ရဲ့ လုပ်ငန်းများ ပိုမိုတိုးတက်လာစေရန် <br> တာဝန်ယူ ဆောင်ရွက်ပေးပါရစေ...</p>
			<a href="http://ihost.spidernetworkict.com/"target="_blank" class="button-w3ls active mt-4 mb-3"> ပိုမို လေ့လာရန်
								<span class="fa fa-caret-right ml-1" aria-hidden="true"></span>
							</a>
	
			</div>
		</div>
		
	</section>
	<!-- //services -->



	<?php include 'footer.php';?>

</body>

</html>