    <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>
	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Myanmar Foods</li>
		</ol>
	</div>
	<!-- //page details -->
	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Myanmar <span> Foods </span></h3>
				
			</div>

					
			 <div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4"> ဒေါ်သန်းသန်း မြန်မာထမင်းဆိုင် </h3>
					<p class="sub-para">စာပန်းအိမ် ပင်မဆောင်နှင့်မျက်စောင်းထိုး၊<br> မြို့ပတ်လမ်း၊ ရကက(၆)၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959681063200">09 681063200</a></p>
					<p class="sub-para">Open Hours:10AM–8PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1099.6128545512972!2d96.47690350876204!3d22.025022949096577!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbbec4edb40c81907!2sDaw%20Than%20Than%20Myanmar%20Food!5e0!3m2!1sen!2smm!4v1576882188152!5m2!1sen!2smm"  frameborder="0" style="border:0;" allowfullscreen=""></iframe>
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/dawthanthan.jpg" alt="about" class="img-fluid" />
				</div>
			</div>
			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/tutu.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">တူးတူး မြန်မာထမင်းဆိုင်</h3>
					<p class="sub-para">မန္တလေး-လားရှိုးလမ်း ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:10AM–9PM</p>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7396.945932245231!2d96.469916!3d22.0314961!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9e96425cc2b5%3A0x66ebe02ff787f206!2sTu%20Tu%20Myanmar%20Traditional%20Food!5e0!3m2!1sen!2smm!4v1576882373507!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

						
				
				</div>
			</div>
				<br> <br> <br> <br> <br>
				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">ဒေါ်ထွေးခင် မြန်မာထမင်းဆိုင်</h3>
					<p class="sub-para">မန္တလေး-လားရှိုးလမ်း ၊ ပြင်ဦးလွင်မြို့။</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.474729693768!2d96.46489251478174!3d22.031428585463903!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9f578841374d%3A0xa840bcb0d1475902!2sDaw%20Htwe%20Khin%20Myanmar%20Restaurant!5e0!3m2!1sen!2smm!4v1577126288524!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/dawhtwekhin.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/myat.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Myat Burmese Foodie</h3>
					<p class="sub-para">စာပန်းအိမ်ပင်မဆောင်ရှေ့ ၊ ပြင်ဦးလွင်မြို့။</p>
					
					
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					
					<h3 class="title-sub mb-4">ဦးဖြိုး ဒံပေါက်</h3>
					
					<p class="sub-para">အမှတ်(၅)၊ (၆) ရပ်ကွက်၊ မန္တလေး-လားရှိးလမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					
					<p class="sub-para"><a href="tel:+95947130730">09 47130730</a></p>
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/uphyo.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/thuzar.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">သူဇာ ဒံပေါက်</h3>
					<p class="sub-para">ချယ်ရီလွင်ရုပ်ရှင်ရုံအနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+9595142515">09 5142515</a></p>
					
				
				</div>
			</div>
			
					
				</div>
			</div>
		</div>
	</section>
	<!-- //about -->


	    <div class="pagination p1">
      <ul>
        <a href="myanmarfoods1.php"><li><</li></a>
        <a class="is-active" href="#"><li>1</li></a>
        <a href="myanmarfoods1.php"><li>2</li></a>
        <a href="myanmarfoods1.php"><li>></li></a>
      </ul>
    </div>



	<?php include 'footer.php';?>

</body>

</html>
