   <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>

	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Chinese Foods</li>
		</ol>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Chinese <span> Foods </span></h3>
				
			</div>
			<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">စန္ဒာမေ</h3>
					<p class="sub-para">(135)A၊ မြို့ပတ်လမ်း (ကန်တော်လေးလမ်း)၊ရပ်ကွက်ကြီး(၆)၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959777799267">09 777799267</a></p>
					<p class="sub-para">Open Hours:8AM–9PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2199.2958683568286!2d96.47445934206117!3d22.020504132271533!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x32b3cf4c8e152cbf!2sSandar%20May%20Restaurant!5e0!3m2!1sen!2smm!4v1576882988043!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/sandarmay.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/welovespicy.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					<h3 class="title-sub mb-4">We Love Spicy</h3>
					<p class="sub-para"> မြို့ပတ်လမ်း (ကန်တော်လေးလမ်း)၊ရပ်ကွက်ကြီး(၆) ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959972224542">09 972224542</a></p>
					<p class="sub-para">Open Hours:10AM–11PM</p>
					<br>
					<iframe class="foodmaps"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2199.3106498887255!2d96.4727113853408!3d22.019551971065948!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9fe2a54a1ae5%3A0x4f9c64e249047a41!2sWe%20love%20Spicy%20%22Malar%20shan%20Kaw%22!5e0!3m2!1sen!2smm!4v1576883050371!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

						
				
				</div>
			</div>
				<br> <br> <br> <br> <br>
				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">လေငွန် စားပွဲရုံ</h3>
					<p class="sub-para"> မန္တလေး-လားရှိုးလမ်း ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:10AM–9PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8796.89956626651!2d96.46576421433079!3d22.025075599755322!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb696a6487dd386e3!2sLay%20Ngoon%20Restaurant!5e0!3m2!1sen!2smm!4v1576883119181!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/layngoon.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/oneone.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">ဝမ်းဝမ်း </h3>
					<p class="sub-para"> မြို့လည် ရွှေစည်းခုံ ကျောင်းတိုက်အနီး ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:8AM–10:30PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.359103141303!2d96.4746928239969!3d22.035854683814712!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ebe8dece8cb%3A0x50d32d2a517d2378!2sWang%20Wang%202%20Beerstation!5e0!3m2!1sen!2smm!4v1576883647897!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
					
						
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Happy</h3>
					<p class="sub-para"> မန္တလေး-လားရှိုးလမ်း ၊ ပြင်ဦးလွင်မြို့။</p>
			
						
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/happy.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/chimchim.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Chim Chim </h3>
					<p class="sub-para"> မြမာလာ၊ ပုညလမ်း ၊ ပြင်ဦးလွင်မြို့။</p>
					
					
						
				</div>
			</div>
		
		
					
				</div>
			</div>
		</div>
	</section>
	<!-- //about -->


		    <div class="pagination p1">
      <ul>
        <a href="chinesefoods1.php"><li><</li></a>
        <a class="is-active" href="#"><li>1</li></a>
        <a href="chinesefoods1.php"><li>2</li></a>
        <a href="chinesefoods1.php"><li>></li></a>
      </ul>
    </div>




	<?php include 'footer.php';?>

</body>

</html>