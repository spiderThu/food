   <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>

	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Cafes</li>
		</ol>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Ca<span>fes </span></h3>
				
			</div>
			<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Hiromi Cafe</h3>
					<p class="sub-para">ဒေလီယာလမ်း၊ ဒေလီယာမိုတယ် အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:8AM–6PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.946315206373!2d96.4727504147814!3d22.013367885473393!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9c1e4f428faf%3A0x3b6ac1dc191165e5!2sHiromi%20Cafe!5e0!3m2!1sen!2smm!4v1577128630284!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					


					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/hiromi.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/crushme.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Crush Me Juice Bar</h3>
					<p class="sub-para">မန္တလေး လာရှိုးလမ်း၊ KBZ ဘဏ် (၃) ရှေ့၊ ညံတော တရုတ်ကျောင်း လမ်း ကြား၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+95943066754">09 43066754</a></p>
					<p class="sub-para">Open Hours:9AM–9PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.4602713723566!2d96.47591541478175!3d22.03198208546365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9f6bb1f65cb7%3A0x4bd9cc08551b65c6!2sCrush%20Me%20Juice%20Bar!5e0!3m2!1sen!2smm!4v1577128689573!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

						
				
				</div>
			</div>
				<br> <br> <br> <br> <br>
				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					
					<h3 class="title-sub mb-4">Fudo Bakery</h3>
					<p class="sub-para">အမှတ်(၃၅)၊ မြို့ပတ်လမ်း၊ ရပ်ကွက်ကြီး(၆)၊ စာပန်းအိမ်ပင်မဆောင်အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:9AM–8PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5230.682599662653!2d96.47740845539245!3d22.0246742375532!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ea4bee8c82b%3A0x2a0430bf1ebf11cc!2sFudo%20Cake%2C%20Ice-cream%2C%20Bakery%20%26%20Hotpot%20Cafe!5e0!3m2!1sen!2smm!4v1577128742862!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				
					

					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/fudo.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/cafegenius.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Cafe Genius</h3>
					<p class="sub-para">ပြင်ဦးလွင် စင်တာ ဒုတိယထပ်၊ ပြင်ဦးလွင်မြို့။</p>
					
					
						
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Taste Of POL</h3>
					<p class="sub-para">အ.လ.က (၁) တိုက်​ခန်​း၊ ဘူတာလမ်​း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959265899370">09 265899370</a></p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.3934425380553!2d96.46459051478178!3d22.03454028546232!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9ff8c5827039%3A0x94c9df2334098843!2sTaste%20Of%20POL%20Burger%20And%20Cafe!5e0!3m2!1sen!2smm!4v1577128803172!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
						
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/taste.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/zinyaw.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">ဇင်ယော် အအေးနှင့်စားဖွယ်စုံ</h3>
					<p class="sub-para">(၈)ကွက်(၉)လမ်း၊ AGDဘဏ်ဘေးလမ်း၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+95943104548">09 43104548</a></p>
					
				
				</div>
			</div>

	</section>
	<!-- //about -->


		    <div class="pagination p1">
      <ul>
        <a href="cafes.php"><li><</li></a>
        <a href="#"><li>1</li></a>
        <a class="is-active" href="#"><li>2</li></a>
        <a href="cafes2.php"><li>3</li></a>
        <a href="cafes2.php"><li>></li></a>
      </ul>
    </div>




	<?php include 'footer.php';?>

</body>

</html>