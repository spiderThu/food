   <!--
   Author: iHost
   Author URL: http://ihost.spidernetworkict.com/
   -->

<!DOCTYPE html>
<html>

<head>
	<title>Food Guide In Pyin Oo Lwin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<link rel="SHORTCUT ICON" href="img/spidernetwork.png">
	<meta name="keywords"
		content="Food Guide In Pyin Oo Lwin Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- Web-Fonts -->
	<link
		href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">
	<!-- //Web-Fonts -->
</head>

<body>
<?php include 'header.php';?>

	<!-- banner -->
	<div class="main-banner-2">

	</div>
	<!-- //banner -->
	<!-- page details -->
	<div class="breadcrumb-agile bg-light py-2">
		<ol class="breadcrumb bg-light m-0">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page">Cafes</li>
		</ol>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="w3ls-bnrbtm py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<div class="title-section text-center mb-md-5 mb-4">
				<h3 class="w3ls-title mb-3"> Ca<span>fes </span></h3>
				
			</div>
			<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">The Club Terrace - လေညှင်းသာ</h3>
					<p class="sub-para">အမှတ်၂၅ ၊ ဂေါက်ကလပ်လမ်း ၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+9592045247">09 2045247</a></p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.877772789988!2d96.45988721478147!3d22.01599378547196!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9e9d5f20495d%3A0xddaeda7e0f453207!2sThe%20Club%20Terrace%20Food%20Lounge!5e0!3m2!1sen!2smm!4v1576515254611!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/theclubterrace.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/bristakhine.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">Brista Khine</h3>
					<p class="sub-para">ဆိုင်ခန်းအမှတ် (၉/၁၀)၊ ဆောက်လုပ်ရေးတိုက်တန်း၊ သစ်တောဧည့်ရိပ်သာရှေ့၊ ဇီဝကလမ်း၊ ရပ်ကွက်ကြီး (၅)၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+95933144369">09 33144369</a></p>
					<p class="sub-para">Open Hours:7:30AM–7:30PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.79845602898!2d96.46182971478152!3d22.01903208547037!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9e9c53f0ab61%3A0xde8feaa049afdf64!2sBarista%20Khine!5e0!3m2!1sen!2smm!4v1577127522371!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

						
				
				</div>
			</div>
				<br> <br> <br> <br> <br>
							<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Nan Myaing</h3>
					<p class="sub-para">မန္တလေး-လားရှိုးလမ်း၊ စစ်တက္ကသိုလ်အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para">Open Hours:8AM–8PM</p>
					<br>
					<iframe  class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.807562204128!2d96.44955331478148!3d22.018683285470548!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9e805d975e9f%3A0x5f69c1bcb1f4394d!2sNan%20Myaing%20Cafe!5e0!3m2!1sen!2smm!4v1577127810460!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/nanmyaing.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/maysong.jpg" alt="about" class="img-fluid" />
				</div>

				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					
						<h3 class="title-sub mb-4">May Song Korea Coffee Shop</h3>
					<p class="sub-para">လမ်း(၂၀)၊ ရပ်ကွက်ကြီး (၂)၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959783337124">09 783337124</a></p>
					<p class="sub-para">Open Hours:9AM–8PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.404213706594!2d96.48123431478177!3d22.03412798546241!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9eb741bb5c3b%3A0xdcc672ecfc9b7140!2sMay%20Song%20Korea%20Coffee%20Shop!5e0!3m2!1sen!2smm!4v1577127847579!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

						
				
				</div>
			</div>
				<br> <br> <br> <br> <br>
				<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">Cafe' May Myo</h3>
					<p class="sub-para">(၆) လမ်း၊ (၇) ကွက်၊ ခဲလယ်ကျောင်းလမ်း၊ နှင်းဆီခိုင် ဟိုတယ် နှင့် UAB Bank အနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959422227758">09 422227758</a></p>
					<p class="sub-para">Open Hours:8AM–9PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.509007727672!2d96.46538501478169!3d22.030116285464636!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9fddca2ebacd%3A0xdf839391eac5e4ee!2sMaymyo%20Cafe!5e0!3m2!1sen!2smm!4v1577128022324!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					

					
						
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/cafemaymyo.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/phoewa.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					<h3 class="title-sub mb-4">Phoe Wa Cafe</h3>
					<p class="sub-para">တရုတ်ဘုံကျောင်းအနီး၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959402651395">09 402651395</a></p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1307.677559848877!2d96.47776525251685!3d22.023925796970797!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9fda8b1989af%3A0x4e7f7e2b7aa588fb!2sPHOE%20WA%20Handmade%20Bakery%20%26%20Cafe!5e0!3m2!1sen!2smm!4v1577128202837!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
						
					
						
				</div>
			</div>
			<br> <br> <br> <br> <br>
					<div class="row">
				<div class="col-lg-6 pr-xl-5 mt-xl-2 mt-lg-0">
					<h3 class="title-sub mb-4">For You Cafe</h3>
					<p class="sub-para">NO.(BB-2) ၊မန္တလေး-လားရှိုး လမ်းမကြီးဘေး ၊တပ်မတော်နည်းပညာတက္ကသိုလ်ရှေ့ ၊ ရပ်ကွက်ကြီး (၁၀)၊ ပြင်ဦးလွင်မြို့။</p>
					<p class="sub-para"><a href="tel:+959420033828">09 420033828</a></p>
					<p class="sub-para">Open Hours:8AM–10PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.216535993535!2d96.49469021478188!3d22.041310885458696!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc99a9adbdc3d7%3A0xd812259e13ed4d39!2sFor%20You%20Caf%C3%A9%20%26%20Restaurant!5e0!3m2!1sen!2smm!4v1577128257933!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
						
					
				</div>
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/foryou.jpg" alt="about" class="img-fluid" />
				</div>
			</div>

			<br> <br> <br> <br> <br>
			<div class="row">
				<div class="col-lg-6 text-center mt-lg-0 mt-4">
					<img src="img/bamboo.jpg" alt="about" class="img-fluid" />
				</div>
				<div class="col-lg-6 text-left mt-lg-0 mt-4">
					<h3 class="title-sub mb-4">Bamboo Cafe And Restaurant</h3>
					<p class="sub-para">အမျိုးသားကန်တော်ကြီး ဥယျာဉ်အတွင်းတွင် ဖွင့်လှစ်ထားပါသည်။</p>
					<p class="sub-para"><a href="tel:+959422227758">09 422227758</a></p>
					<p class="sub-para">Open Hours:6AM–10PM</p>
					<br>
					<iframe class="foodmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3699.4563242436748!2d96.46888561478102!3d21.993819785483627!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9c3fc49ca563%3A0x427025c20be99bf5!2sBamboo%20Bar%20%26%20Cafe!5e0!3m2!1sen!2smm!4v1577128458870!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					
				
				</div>
			</div>

	</section>
	<!-- //about -->


		    <div class="pagination p1">
      <ul>
        <a href="cafes2.php"><li><</li></a>
        <a class="is-active" href="#"><li>1</li></a>
        <a href="cafes1.php"><li>2</li></a>
        <a href="cafes2.php"><li>3</li></a>
        <a href="cafes1.php"><li>></li></a>
      </ul>
    </div>




	<?php include 'footer.php';?>

</body>

</html>